<?php


namespace examples\routing;


class BaseController
{
	/**
	 * @var Router
	 */
	public $router;

	public function run()
	{
		$action=$this->router->getAction();
		$this->__call($action,[]);
	}

	public function addAction($route, $callable)
	{
		if (!$this->router) {
			$this->router = new Router();
		}
		$this->router->addAction($route, $callable);
	}

	public function setFallback($route, $callable)
	{
		if (!$this->router) {
			$this->router = new Router();
		}
		$this->router->setFallback($callable);
	}
}