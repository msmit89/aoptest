<?php


namespace examples\routing;


class Router
{
	public $routes=[];

	public $fallback;

	public function addAction($route, $callable)
	{
		$this->routes[$route] = $callable;
	}

	public function setFallback($callable)
	{
		$this->fallback = $callable;
	}

	public function getAction()
	{
		$route = @array_keys($_GET)[0];
		if (!$route) {
			$route = "index";
		}

		$routes=$this->routes;
		if (isset($routes[$route])) {
			return $routes[$route];
		}

		return $this->fallback;

	}


}