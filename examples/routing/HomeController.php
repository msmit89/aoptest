<?php


namespace examples\routing;


/**
 * @aop
 */
class HomeController extends BaseController
{
	/**
	 * @Action(index)
	 */
	public function index()
	{
		echo "homepage";
	}

	/**
	 * @Action(mark)
	 */
	public function mark()
	{
		echo "mark";
	}


	/**
	 * @Fallback
	 */
	public function fallback()
	{
		echo "fallback";
	}


}