<?php

namespace examples\routing;

use lib\aop\aspect\BaseAspect;

class Fallback extends BaseAspect
{
	function prepareMethod($method, $instance)
	{
		$instance->setFallback($this->route, $method);
	}

}