<?php

namespace examples\routing;

use lib\aop\aspect\BaseAspect;

class Action extends BaseAspect
{
	public $route;

	function __construct($route)
	{
		$this->route = $route;
	}

	function prepareMethod($method, $instance)
	{
		$instance->addAction($this->route, $method);
	}

}