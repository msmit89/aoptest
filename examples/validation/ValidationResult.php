<?php namespace examples\validation;

class ValidationResult
{


	public $result;
	public $errorCount = 0;

	public function validateError($field, $reason)
	{
		if (empty($this->result[$field]["status"]) || $this->result[$field]["status"] == "success") {
			$this->errorCount++;
		}

		return $this->result[$field] = ["status" => "fail", "reason" => $reason];
	}

	public function validateSuccess($field)
	{
		if (isset($this->result[$field]["status"]) && $this->result[$field]["status"] != "success") {
			$this->errorCount--;
		}
		return $this->result[$field] = ["status" => "success"];
	}


}