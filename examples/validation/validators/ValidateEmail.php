<?php namespace examples\validation\validators;

use lib\aop\aspect\BaseAspect;

class ValidateEmail extends BaseAspect
{
	public function setField($fieldName, $value, $instance)
	{
		if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
			$instance->validateError($fieldName, "Not a valid email address");
		} else {
			$instance->validateSuccess($fieldName);
		}
		return $value;
	}


}