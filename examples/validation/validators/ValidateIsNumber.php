<?php namespace examples\validation\validators;

use lib\aop\aspect\BaseAspect;

class ValidateIsNumber extends BaseAspect
{

	public function setField($fieldName, $value, $instance)
	{

		if (!is_numeric($value)) {
			$instance->validateError($fieldName, "Not a number");
		} else {
			$instance->validateSuccess($fieldName);
		}
		return $value;
	}

}