<?php namespace examples\validation\validators;

use lib\aop\aspect\BaseAspect;

class ValidateNotEmpty extends BaseAspect
{

	public function setField($fieldName, $value, $instance)
	{

		if (empty($value)) {
			$instance->validateError($fieldName, "Cannot be empty");
		} else {
			$instance->validateSuccess($fieldName);
		}
		return $value;
	}

}