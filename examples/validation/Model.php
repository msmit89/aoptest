<?php namespace examples\validation;
use examples\validation\validators\ValidateEmail;
use examples\validation\validators\ValidateIsNumber;
use examples\validation\validators\ValidateNotEmpty;

/**
 * @aop
 */
class Model
{
	use ValidatableTrait;

	/**
	 * @ValidateEmail
	 */
	public $email;

	/**
	 * @ValidateNotEmpty
	 */
	public $name;

	/**
	 * @ValidateIsNumber
	 */
	public $age;
}