<?php namespace examples\validation;

trait ValidatableTrait
{

	/**
	 * @var ValidationResult
	 */
	public $validationResults;

	public function validateError($field, $reason)
	{
		if (empty($this->validationResults)) {
			$this->validationResults = new ValidationResult();
		}
		return $this->validationResults->validateError($field, $reason);
	}

	public function validateSuccess($field)
	{
		if (empty($this->validationResults)) {
			$this->validationResults = new ValidationResult();
		}
		return $this->validationResults->validateSuccess($field);
	}

	public function getValidationResults()
	{
		return $this->validationResults->result;
	}

	public function getErrorCount(){
		if (empty($this->validationResults)) {
			return 0;
		}
		return $this->validationResults->errorCount;
	}
}

?>