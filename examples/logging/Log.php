<?php namespace examples\logging;

use lib\aop\aspect\BaseAspect;


class Log extends BaseAspect
{

	private $file;

	function __construct($file)
	{
		$this->file = "log/".$file;
	}

	public function postMethodCall(string $method, $returnValue, $instance)
	{
		$this->log("Method call success:" . $method) . "with return value: " . $returnValue;
		return $returnValue;
	}

	public function methodException($method, $ex, $instance)
	{
		$this->log("Method call failed:" . $method) . " with message" . $ex->getMessage();
		return false;
	}

	function log($text)
	{
		if(!is_dir("log")){
			if (!mkdir("log") && !is_dir("log")) {
				throw new \RuntimeException(sprintf('Directory "%s" was not created', "log"));
			}
		}
		$line = date("Y-m-d h:i:s") . ":" . $text."\n";
		file_put_contents($this->file, $line, FILE_APPEND);
	}
}