<?php namespace examples\logging;

/**
 * @aop
 */
class Controller
{
	/**
	 * @Log(controller.log)
	 */
	function successAction()
	{
		return "Good!!";
	}

	/**
	 * @Log(controller.log)
	 */
	function failAction()
	{
		throw new \Exception("Wrong!!");
	}
}