<?php namespace examples\autofill;

/**
 * @aop
 */
class Model
{

	/**
	 * @FromRequest(email)
	 */
	public $email;

	/**
	 * @FromRequest(name)
	 */
	public $name;

	/**
	 * @FromRequest(age)
	 */
	public $age;
}