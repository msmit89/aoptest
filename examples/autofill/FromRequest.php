<?php namespace examples\autofill;

use lib\aop\aspect\BaseAspect;


class FromRequest extends BaseAspect
{

	private $field;

	function __construct($field)
	{
		$this->field = $field;
	}

	function getField($fieldName, $value, $instance)
	{

		if (!$value) {
			$instance->{$fieldName} = $_GET[$this->field];
		}
		return $_GET[$this->field];
	}
}