<?php namespace examples\authorization;

use lib\aop\aspect\BaseAspect;

class Authenticate extends BaseAspect
{

	private $user;

	function __construct($user)
	{
		$this->user = $user;
	}

	public function preConstruct(array $params, $instance): array
	{
		if ($_GET["user"] != $this->user) {
			die("nope only ?user=".$this->user." is allowed");
		}
		return $params;
	}
}

?>