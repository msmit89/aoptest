<?php namespace examples\authorization;

/**
 * @aop
 * @Authenticate(mark)
 */
class Controller
{
	public function runAction(){
		echo "action runs";
	}
}

?>