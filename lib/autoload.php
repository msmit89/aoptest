<?php

use lib\aop\Injector;
chdir(__DIR__."/../");
include_once("lib/aop/Injector.php");

spl_autoload_register(
	function ($class) {

		$namespace = substr($class, 0, strripos($class, "\\"));
		$className = substr($class, strripos($class, "\\") + 1);

		$path = str_replace("\\", "/", $class);
		$file = $path . ".php";

		if (file_exists($file)) {
			$injector = new Injector($namespace, $className, $file);
			$injector->inject();
			return true;
		}
		return false;
	}
)

?>