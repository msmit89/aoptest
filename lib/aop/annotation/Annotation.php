<?php

namespace lib\aop\annotation;

use lib\aop\aspect\BaseAspect;

class  Annotation
{
	public  $class;
	public  $properties;
	private $aspectInstance;
	private $instanceInitialized;

	private static $aspectExists;

	public function __construct($class, $properties = [])
	{
		$this->class      = $class;
		$this->properties = $properties;
	}

	static public function __set_state(array $arr): Annotation
	{
		return new Annotation($arr["class"], $arr["properties"]);
	}

	public function getAspect(): ?BaseAspect
	{
		if (!$this->instanceInitialized) {
			if ($this->aspectExists()) {
				$className            = $this->class;
				$this->aspectInstance = new $className(...$this->properties);
			}
			$this->instanceInitialized = true;
		}
		return $this->aspectInstance;
	}

	private function aspectExists(): bool
	{
		if (!isset(self::$aspectExists[$this->class])) {
			if (class_exists($this->class) && is_subclass_of($this->class, BaseAspect::class)) {
				$className                        = $this->class;
				self::$aspectExists[$this->class] = true;
			} else {
				self::$aspectExists[$this->class] = false;
			}
		}

		return self::$aspectExists[$this->class];
	}


}