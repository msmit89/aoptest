<?php

namespace lib\aop\annotation;


class  AnnotationHelper
{

	static public function getAnnotationsForClass($class): array
	{
		$r   = new \ReflectionClass($class);
		$doc = $r->getDocComment();
		return self::getAnnotationsForDocComment($class, $doc);

	}

	static public function getAnnotationsForMethod($class, string $method): array
	{
		$r   = new \ReflectionMethod($class, $method);
		$doc = $r->getDocComment();
		return self::getAnnotationsForDocComment($class, $doc);
	}

	static public function getAnnotationsForField($class, string $field): array
	{
		$r   = new \ReflectionProperty($class, $field);
		$doc = $r->getDocComment();
		return self::getAnnotationsForDocComment($class, $doc);
	}


	/**
	 * @param $doc
	 * @param $annotations
	 * @return array|mixed
	 */
	static private function getAnnotationsForDocComment($class, $doc): array
	{
		$annotations = [];
		preg_match_all('#@(.*?)[\r]{0,1}\n#s', $doc, $annotations);
		if (isset($annotations[1])) {
			return self::parseAnnotations($class, $annotations[1]);
		}
		return [];
	}

	static private function parseAnnotations($class, array $annotations): array
	{
		$result = [];
		foreach ($annotations as $annotation) {
			$matches = [];
			preg_match_all('#(.*?)\((.*?)\)#s', $annotation, $matches);
			if (!empty($matches[1])) {
				$result[] = self::createAnnotation(
					$class, $matches[1][0], !empty($matches[2][0]) ? explode(",", $matches[2][0]) : []
				);
			} else {
				$result[] = self::createAnnotation($class, $annotation);

			}

		}
		return $result;
	}

	private static function createAnnotation($class, $annotationClass, array $annotationArgs = []): Annotation
	{
		$reflectionClass = new ExtendedReflectionClass($class);
		$useStatements   = $reflectionClass->getUseStatements();
		if (!empty($useStatements)) {
			foreach ($useStatements as $statement) {
				if ($statement["as"] == $annotationClass) {
					return new Annotation("\\" . $statement["class"], $annotationArgs);
				}
			}
		}
		$reflectionClass->getNamespaceName();
		return new Annotation("\\" . $reflectionClass->getNamespaceName() . "\\" . $annotationClass, $annotationArgs);

	}
}