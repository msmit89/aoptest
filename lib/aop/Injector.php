<?php

namespace lib\aop;

use lib\aop\proxy\OriginalToProxyableParser;
use lib\aop\proxy\ProxyCreator;

class  Injector
{

	public $file;
	public $namespace;
	public $class;

	/**
	 * Injector constructor.
	 * @param $file
	 * @param $namespace
	 * @param $class
	 */
	public function __construct($namespace, $class, $file)
	{
		$this->file      = $file;
		$this->namespace = $namespace;
		$this->class     = $class;
	}

	public function inject()
	{
		if ($this->needsUpdate()) {
			$content = file_get_contents($this->file);
			$content = $this->parseFileContent($content);
			$this->cleanupFiles();
			$this->saveFile($content);
			$this->createProxy($content);
		}
		if (file_exists($this->getProxyFileName())) {
			include_once($this->getProxyFileName());
		} else {
			include_once($this->getOriginalFileName());
		}
	}

	private function needsUpdate()
	{
		$buildName = $this->getOriginalFileName();
		return !file_exists($buildName) || \filemtime($this->file) > \filemtime($buildName);
	}

	private function getOriginalFileName()
	{
		return "build/" . $this->file;
	}

	private function getProxyFileName()
	{
		return "build/" . $this->file . ".proxy.php";
	}


	private function parseFileContent(string $content)
	{
		if ($this->hasProxy($content)) {
			$contentParser = new OriginalToProxyableParser($this->namespace, $this->class);
			$content       = $contentParser->parse($content);
		}
		return $content;
	}

	private function saveFile(string $content)
	{
		$file   = $this->getOriginalFileName();
		$folder = substr($file, 0, strripos($file, "/"));
		if (!is_dir($folder)) {
			if (!mkdir($folder,0777,true) && !is_dir($folder)) {
				throw new \RuntimeException(sprintf('Directory "%s" was not created', "build"));
			}
		}


		file_put_contents($file, $content);
	}

	private function createProxy($content)
	{
		if ($this->hasProxy($content)) {
			$proxyCreator=new ProxyCreator($this->namespace, $this->class, $this->getOriginalFileName(), $this->getProxyFileName());
			$proxyCreator->createProxy();
		}
	}

	/**
	 * @param $content
	 * @return bool
	 */
	private function hasProxy($content): bool
	{
		return strpos($content, '@aop') !== false;
	}

	private function cleanupFiles()
	{
		if(file_exists($this->getProxyFileName())){
			unlink($this->getProxyFileName());
		}
		if(file_exists($this->getOriginalFileName())){
			unlink($this->getOriginalFileName());
		}
	}


}

?>