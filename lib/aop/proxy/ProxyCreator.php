<?php

namespace lib\aop\proxy;

use lib\aop\annotation\Annotation;
use lib\aop\annotation\AnnotationHelper;

class  ProxyCreator
{

	private $namespace;
	private $class;
	private $file;
	private $originalFile;

	public function __construct($namespace, $class, $originalFile, $file)
	{
		$this->namespace    = $namespace;
		$this->class        = $class;
		$this->originalFile = $originalFile;
		$this->file         = $file;
	}

	public function createProxy(): void
	{
		$content = $this->createClassStr();
		file_put_contents($this->file, $content);
	}

	private function createClassStr(): string
	{
		$classStr = file_get_contents("lib/aop/proxy/proxy.tpl");
		$classStr = str_replace("{{origFile}}", $this->originalFile, $classStr);
		$classStr = str_replace("{{class}}", $this->class, $classStr);
		$classStr = str_replace("{{namespace}}", $this->namespace, $classStr);
		$classStr = str_replace("{{metadata}}", $this->createMetaDataStr(), $classStr);
		return $classStr;
	}


	private function createMetaDataStr()
	{

		require_once($this->originalFile);

		$class = $this->namespace . "\\OriginalNonAop" . $this->class;

		$resultData['annotations']['class']   = AnnotationHelper::getAnnotationsForClass($class);
		$resultData['annotations']['methods'] = $this->getMethodAnnotations(
			$class, $resultData['annotations']['class']
		);
		$resultData['annotations']['fields']  = $this->getFieldAnnotations($class, $resultData['annotations']['class']);

		return $this->metaDataToString($resultData);
	}

	private function getFieldAnnotations(string $class, $classAnnotations)
	{
		$reflectionClass = new \ReflectionClass($class);
		foreach ($reflectionClass->getProperties() as $property) {
			$results[$property->getName()] = array_merge(
				AnnotationHelper::getAnnotationsForField($class, $property->getName()), $classAnnotations
			);
		}
		return $results;
	}

	private function getMethodAnnotations(string $class, $classAnnotations)
	{
		$reflectionClass = new \ReflectionClass($class);
		foreach ($reflectionClass->getMethods() as $method) {
			$results[$method->getName()] = array_merge(
				AnnotationHelper::getAnnotationsForMethod($class, $method->getName()), $classAnnotations
			);
		}
		return $results;
	}

	/**
	 * @param $resultData
	 * @return mixed
	 */
	private function metaDataToString($resultData): string
	{
		$exported = var_export($resultData, true);
		$exported = str_replace(Annotation::class, "\\" . Annotation::class, $exported);
		return $exported;
	}


}