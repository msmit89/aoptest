<?php
      namespace {{namespace}};

      include_once("{{origFile}}");
      class {{class}} extends OriginalNonAop{{class}} {
            use \lib\aop\proxy\ProxyTrait;

            protected function _getMetaData(){
                return {{metadata}};
            }
      }
?>