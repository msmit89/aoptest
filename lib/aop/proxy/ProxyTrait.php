<?php

namespace lib\aop\proxy;

use lib\aop\aspect\AspectHandler;

trait ProxyTrait
{
	/**
	 * @var AspectHandler
	 */
	public $_aspectHandler;
	public $_metadata;

	static $metaData = [];

	function __construct()
	{
		$this->initAspectHandler();
		$args = $this->_aspectHandler->preConstruct(func_get_args());
		$this->callConstructor($args);
		$this->callPrepareMethods();
		$this->_aspectHandler->postConstruct();
	}

	function __call($name, $arguments)
	{
		try {
			$fullName    = "AopInjectable_" . $name;
			$arguments   = $this->_aspectHandler->preMethodCall($fullName, $arguments);
			$returnValue = parent::{$fullName}(...$arguments);
			return $this->_aspectHandler->postMethodCall($fullName, $returnValue);
		} catch (\Exception $exception) {
			return $this->_aspectHandler->methodException($fullName, $exception);
		}
	}

	function __get($name)
	{
		$fullName = "AopInjectable_" . $name;
		return $this->_aspectHandler->getField($fullName, $this->{$fullName});
	}

	function __set($name, $value)
	{
		$fullName          = "AopInjectable_" . $name;
		$this->{$fullName} = $this->_aspectHandler->setField($fullName, $value);
	}

	function __isset($name)
	{
		$fullName = "AopInjectable_" . $name;
		return isset($this->{$fullName});
	}

	/**
	 * @param array $args
	 */
	private function callConstructor(array $args): void
	{
		if (method_exists(get_parent_class($this), '__construct')) {
			parent::__construct(...$args);
		}
	}

	private function initAspectHandler(): void
	{
		$this->_aspectHandler = new AspectHandler($this->_getMetaData(), $this);
	}

	private function callPrepareMethods(): void
	{
		$this->_aspectHandler->prepareMethods();
		$this->_aspectHandler->prepareFields();
	}

}