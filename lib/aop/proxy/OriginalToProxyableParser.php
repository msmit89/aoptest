<?php

namespace lib\aop\proxy;

class  OriginalToProxyableParser
{

	private $namespace;
	private $class;

	/**
	 * AopFileContentParser constructor.
	 * @param $namespace
	 * @param $class
	 */
	public function __construct($namespace, $class)
	{
		$this->namespace = $namespace;
		$this->class     = $class;
	}

	function parse(string $content): string
	{
		$content = $this->trimContent($content);
		$content = $this->renameClass($content);
		$content = $this->handleScopes($content);
		return $content;
	}


	/**
	 * @param $content
	 * @return string
	 */
	private function trimContent(string $content): string
	{
		$content = trim($content, '?> \n\t\r');
		return $content;
	}

	private function renameClass(string $content): string
	{
		return str_replace('class ' . $this->class, $this->getAopInjectableClassName(), $content);
	}

	/**
	 * @return string
	 */
	private function getAopInjectableClassName(): string
	{
		return 'class OriginalNonAop' . $this->class;
	}

	private function handleScopes(string $content): string
	{
		$result = str_replace(['var ', 'public ', 'private '], 'protected ', $content);
		$result = str_replace('protected $', 'protected $AopInjectable_', $result);
		return str_replace('function ', 'function AopInjectable_', $result);
	}
}