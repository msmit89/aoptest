<?php

namespace lib\aop\aspect;

class BaseAspect
{

	public function init()
	{
	}

	public function preConstruct(array $params, $instance): array
	{
		return $params;
	}


	public function postConstruct($instance)
	{

	}

	public function preMethodCall(string $method, array $params, $instance): array
	{
		return $params;
	}

	public function postMethodCall(string $method, $returnValue, $instance)
	{
		return $returnValue;
	}

	public function methodException($method, $ex, $instance)
	{
		return false;
	}

	public function prepareMethod($method, $instance)
	{

	}

	public function prepareField($field, $instance)
	{
	}

	public function getField($fieldName, $value, $instance)
	{
		return $value;
	}

	public function setField($fieldName, $value, $instance)
	{
		return $value;
	}
}