<?php

namespace lib\aop\aspect;

use lib\aop\annotation\Annotation;

class AspectHandler
{
	protected $classMeta;
	protected $classInstance;

	public function __construct($classMeta, $classInstance)
	{
		$this->classMeta     = $classMeta;
		$this->classInstance = $classInstance;
	}

	public function preConstruct(array $params): array
	{
		if (!$this->classMeta["annotations"]["class"]) {
			return $params;
		}

		/**
		 * @var Annotation $annotation
		 */
		$annotations = @$this->classMeta["annotations"]["class"];
		if (is_array($annotations)) {
			foreach ($annotations as $annotation) {
				$aspect = $annotation->getAspect();
				if ($aspect) {
					$params = $aspect->preConstruct($params, $this->classInstance);
				}
			}
		}
		return $params;
	}


	public function postConstruct(): void
	{
		if (!$this->classMeta["annotations"]["class"]) {
			return;
		}
		$annotations = $this->classMeta["annotations"]["class"];
		$annotations = array_reverse($annotations);
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				$aspect->postConstruct($this->classInstance);
			}
		}
	}

	public function preMethodCall(string $method, array $params): array
	{
		if (!$this->classMeta["annotations"]["methods"][$method]) {
			return $params;
		}

		$annotations = $this->classMeta["annotations"]["methods"][$method];
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				$params = $aspect->preMethodCall($this->originalName($method), $params, $this->classInstance);
			}
		}

		return $params;
	}


	public function postMethodCall(string $method, $returnValue)
	{

		if (!$this->classMeta["annotations"]["methods"][$method]) {
			return $returnValue;
		}

		$annotations = $this->classMeta["annotations"]["methods"][$method];
		$annotations = array_reverse($annotations);
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				$returnValue = $aspect->postMethodCall($this->originalName($method), $returnValue, $this->classInstance);
			}
		}

		return $returnValue;
	}

	public function methodException(string $method, $exception)
	{

		if (!$this->classMeta["annotations"]["methods"][$method]) {
			throw $exception;
		}

		$annotations = $this->classMeta["annotations"]["methods"][$method];
		$annotations = array_reverse($annotations);
		$continue    = false;
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				if ($aspect->methodException($this->originalName($method), $exception, $this->classInstance)) {
					$continue = true;
				}
			}
		}

		if (!$continue) {
			throw $exception;
		}
	}

	public function getField(string $fieldName, $value)
	{
		if (!$this->classMeta["annotations"]["fields"][$fieldName]) {
			return $value;
		}

		$annotations = $this->classMeta["annotations"]["fields"][$fieldName];
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				$value = $aspect->getField($this->originalName($fieldName), $value, $this->classInstance);
			}
		}

		return $value;
	}

	public function setField(string $fieldName, $value)
	{
		if (!$this->classMeta["annotations"]["fields"][$fieldName]) {
			return $value;
		}

		$annotations = $this->classMeta["annotations"]["fields"][$fieldName];
		/**
		 * @var Annotation $annotation
		 */
		foreach ($annotations as $annotation) {
			$aspect = $annotation->getAspect();
			if ($aspect) {
				$value = $aspect->setField($this->originalName($fieldName), $value, $this->classInstance);
			}
		}

		return $value;
	}

	public function prepareMethods()
	{
		if (isset($this->classMeta["annotations"]["methods"])) {
			foreach ($this->classMeta["annotations"]["methods"] as $method => $annotations) {
				/**
				 * @var Annotation $annotation
				 */
				foreach ($annotations as $annotation) {
					$aspect = $annotation->getAspect();
					if ($aspect) {
						$aspect->prepareMethod($this->originalName($method), $this->classInstance);
					}
				}
			}
		}
	}

	public function prepareFields()
	{
		if (isset($this->classMeta["annotations"]["fields"])) {
			foreach ($this->classMeta["annotations"]["fields"] as $field => $annotations) {
				/**
				 * @var Annotation $annotation
				 */
				foreach ($annotations as $annotation) {
					$aspect = $annotation->getAspect();
					if ($aspect) {
						$aspect->prepareField($this->originalName($field), $this->classInstance);
					}
				}
			}
		}
	}

	protected function originalName($name)
	{
		return str_replace("AopInjectable_", "", $name);
	}
}